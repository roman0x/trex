#include <stdio.h>
#include <curl/curl.h>
#include <stdlib.h>
#include "btrx.h"
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <syslog.h>
#include <signal.h>
#include <getopt.h>
#include <errno.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <errno.h>
#include <sys/mman.h>
#include <signal.h>


unsigned char volatile h = 1;

void	sig_handler(int sig)
{
	h = 1;
	printf("[Signal handler]\n");
	(void)sig;
}


int	get_epoch(char *data)
{
	struct	tm t;
	int	idx;
	time_t	tm;

	if (data == NULL || *data == '\0')
		return 0;

	idx = 0;
	while (data[idx])
	{
		if (data[idx] == 'T' && data[idx + 1] == 'i')
			break;
		idx++;
	}
	if (data[idx] == '\0')
		return 0;

	idx += 17;	
	t.tm_mon = (data[idx] - 48) * 10 + (data[idx + 1] - 48) - 1;
	t.tm_mday = (data[idx + 3] - 48) * 10 + (data[idx + 4] - 48);
	t.tm_hour = (data[idx + 6] - 48) * 10 + (data[idx + 7] - 48);
	t.tm_min = (data[idx + 9] - 48) * 10 + (data[idx + 10] - 48);
	t.tm_sec = (data[idx + 12] - 48) * 10 + (data[idx + 13] - 48);
	t.tm_year = 118;// 2018 - 1900 = 118
	tm = mktime(&t);

	return (int)tm;
}


void	update_intervals(int fmin, t_volume *vol)
{
	(void)vol;	
	if (fmin == 0)
	{
		//vol->fifteen[0].open = vol->five_min[0].open;
	//	vol->fifteen[0].high = vol->five_min.high;
		//vol->fifteen[0].low = vol->five_min[0].low;
		return;
	}
	//if (vol->five_min.high > vol->fifteen[0].high)
	//	vol->fifteen[0].high = vol->five_min.high;
	//if (vol->five_min[fmin].low < vol->fifteen[fmin].low)
	//	vol->fifteen[fmin].low = vol->five_min[fmin].low;


/*	if (vol->fifteen[1].high * 1.05 > vol->fifteen[0].close) // spot 5% increase	
		printf("   %s: >5%% \n", tickers[vol->idx]);
	if (fmin == 2)
	{	
		vol->fifteen[0].close = vol->five_min.close;
		vol->fifteen[1].high = vol->fifteen[0].high;
	}
*/
}

void	write_to_file(t_volume *vol)
{
	static int	fmin_interval = 0;
	t_volume	*n;
	int		idx;

	idx = 0;
	n = vol - vol->idx;
	while (idx < MAX_TICKERS)
	{
		if ((n + idx)->five.fd == NULL)
		{
			printf(" File can't be opened\n");
			(n + idx)->times += 300; // +5 min
			(n + idx)->five.vol = 0;
			idx++;
			continue;
		}
		//printf("%s: ", tickers[idx]);
		fprintf((n + idx)->five.fd, "%d;%d;", (n + idx)->times - 300, (int)(n + idx)->five.vol);
		fprintf((n + idx)->five.fd, "%.8lf;%.8lf;%.8lf;%.8lf-%ld\n", (n + idx)->five.open, \
			(n + idx)->five.high ,(n + idx)->five.low ,(n + idx)->five.close, (n + idx)->five.trades);
		fflush((n + idx)->five.fd);
		//update_intervals(fmin_interval, n + idx);
		(n + idx)->times += 300; // +5 min
		(n + idx)->com_five = 0; // 5min candle incomplete
		memset(&(n + idx)->five, 0, sizeof(t_candle));
		(n + idx)->five.low = 1;
		idx++;
	}
	if (fmin_interval == 2)
		fmin_interval = 0;
	else
		fmin_interval++;
}

void	 get_quantity(char *buff, size_t *idx, double *ret)
{
	*idx += 40;
	while (buff[*idx] != 'Q')
		(*idx)++;
	*idx += 10;
	if (*(buff + *idx) == '0')
		*ret = atof(buff + *idx);
	else
		*ret = atoi(buff + *idx);
}

void	get_price(char *buff, size_t *idx, double *ret)
{
	*idx += 11;
	while (buff[*idx] != 'P')
		(*idx)++;
	*idx += 7;

	*ret = atof(buff + *idx);
}


int	get_vol(input* buff, size_t buff_idx, t_volume *vol, t_tx_data *td)
{
	static int	candle_com = 0;
	size_t		idx;
	int		txid = 0;
	int		first_id;
	int		times = (int)time(NULL);


	if (buff->size < 50)//unsuccessful request
	{
		if (vol->com_five == 0)
		{
			vol->com_five = 1;
			candle_com ++;
		}
		printf(" Failed request for %s \n", tickers[vol->idx]);
		return -1;
	}

	if (candle_com == MAX_TICKERS - 1)
	{
		write_to_file(vol);
		candle_com = 0;
	}
	if (vol->com_five == 1)
		return 0;

	first_id = atoi(buff->data + 45);
	if ((first_id == vol->last_id && vol->times - 1 <= times))
	{
		if (vol->com_five == 0 && buff_idx == 0)
		{
			int ret = get_vol(buff, 1, vol, td);
			if (ret != 2)
				printf("%s closed %d\n", tickers[vol->idx], candle_com);
			vol->com_five = 1;
			candle_com++;	
		}
	}
	if (first_id == vol->last_id)
		return 0;

	idx = 40;
	txid = 0;
	while (buff->data[idx] && idx < buff->size - 1)
	{
		if (buff->data[idx] == 'I' && buff->data[idx + 1] == 'd')
		{
			idx += 4;
			td->curr_id[txid] = atoi(buff->data + idx);
			td->ts[txid] = get_epoch(buff->data + idx + 2);
			
			get_quantity(buff->data, &idx, td->quan + txid);
			get_price(buff->data, &idx, td->price + txid);
	
			if (td->ts[txid] < vol->times - 300) //past current timestamp
			{
				if (txid == 0)
				{
					vol->last_id = first_id;
					return 1;
				}
				txid--;
				break;
			}
			if (td->curr_id[txid] == vol->last_id)
			{
				if (txid == 0)
					return 1;
				txid--;
				break;
			}
			if (txid == 99)
				break;
			idx += 64;
			txid++;
		}
		idx++;
	}
	
	if (vol->five.open_flag == 0 && vol->times - 300 <= td->ts[txid])
	{
		vol->five.open_flag = 1;
		vol->five.open = td->price[txid];
	}

	while (1)
	{
		if (td->ts[txid] >= vol->times - 1)
		{
			vol->com_five = 1;
			candle_com++;
			if (txid == 0)
			{
				vol->last_id = first_id;
				return 0;
			}
			vol->five.close = td->price[(txid != 99) ? txid + 1 : txid];
			vol->five.trades++;
			vol->last_id = td->curr_id[txid];
			vol->five.vol += td->quan[txid + 1];
			printf(" %s %d\n", tickers[vol->idx], candle_com);	
			return 2;
		}
		if (vol->five.high < td->price[txid])  
			vol->five.high = td->price[txid];
		if (vol->five.low > td->price[txid])
			vol->five.low = td->price[txid];
		vol->five.vol += td->quan[txid];
		vol->five.trades++;
		txid--;
		if (txid == -1)
			break;
	}

	vol->five.close = td->price[0];
	vol->last_id = td->curr_id[0];

	if (vol->times <= times)
	{
		vol->com_five = 1;
		candle_com ++;
		printf(" %s %d\n", tickers[vol->idx], candle_com);
		return 2;
	}
	return 3;
}

size_t	write_data(void *buffer, size_t size, size_t nmemb, void *userp)
{

	char	*t;
	input	*in = (input*)userp;

	if (in == NULL || buffer == NULL)
		return 0;
	if (in->size == 0)
	{
		in->size = nmemb;
		in->data = (char*)malloc(sizeof(char) * nmemb);
		memcpy((void*)in->data, buffer, nmemb);
	}
	else
	{
		t = (char*)malloc(sizeof(char) * (in->size + nmemb));
		memcpy(t, in->data, in->size);
		memcpy(t + in->size, buffer, nmemb);
		free(in->data);
		in->size += nmemb;
		in->data = t;
	}

	return nmemb;
	(void)size;
}

int	get_history(char *url, CURL *handle, t_volume *vol, t_tx_data *td)
{
       	CURLcode	res;
	input		*buff;

	
	buff = (input*)malloc(sizeof(input));
	buff->size = 0;
	buff->data = NULL;
		
	curl_easy_setopt(handle, CURLOPT_URL, url);
	curl_easy_setopt(handle, CURLOPT_WRITEFUNCTION, write_data);
	curl_easy_setopt(handle, CURLOPT_WRITEDATA, buff);
	res = curl_easy_perform(handle);
	if (res != CURLE_OK)
	{
		fprintf(stderr, "curl easy perform error\n");
		return (1);
	}
	memset(td, 0, sizeof(t_tx_data));
	get_vol(buff, 0, vol, td);

	free(buff->data);
	buff->data = NULL;
	free(buff);
	buff = NULL;
	
	return 0;

}

t_volume*	init_vol(void)
{
	char		five[50] = "./5m/";
	char		fifteen[50] = "./15m/";
	t_volume	*vol;
	int		idx;

	vol = (t_volume*)malloc(sizeof(t_volume) * MAX_TICKERS);
	memset(vol, 0, sizeof(t_volume) * MAX_TICKERS);

	idx = 0;
	while (idx < MAX_TICKERS)
	{
		strcpy(five + 5, tickers[idx]);
		strcat(five, ".txt");
		strcpy(fifteen + 6, tickers[idx]);
		strcat(fifteen, ".txt");
		(vol + idx)->last_id = 0;
		(vol + idx)->times = STARTING_TS + 300; 
		(vol + idx)->idx = idx;
		memset(&(vol + idx)->fifteen, 0, sizeof(t_candle));
		memset(&(vol + idx)->five, 0, sizeof(t_candle));
		(vol + idx)->five.low = 1;
		(vol + idx)->five.fd = fopen(five, "a");
		(vol + idx)->fifteen.fd = fopen(fifteen, "a");
		(vol + idx)->com_five = 0;
		(vol + idx)->com_fifteen = 0;
		idx++;
	}
	return vol;
}

void	wait_until(int timestamp)
{
	int	ts;

	while (1)
	{	
		ts = (int)time(NULL);
		if (ts == timestamp)
			break;
		usleep(10000);
	}
	printf("%d\n\n", ts);	
}

int	main(int argc, char **argv)
{	
	CURL		*handle;
	int		idx = 0;
	pid_t		pid = 0;
	char		url[128];
	t_volume	*vol;
	t_tx_data	td;
	struct sigaction sighandler;
	sigset_t 	bl;

	if (argc > 1)
	{
		if (argv[1][0] == '-' && argv[1][1] == 'f')
		{	
			pid = fork();
			if (pid > 0)
			{
				printf(" \n Child process %d\n", pid);
				return EXIT_SUCCESS;
			}
			else if (pid == 0)
				printf("\n Child process running\n");
			else
				printf("\n fork error\n");
		}
	}

	// sigaction init
	sighandler.sa_handler = sig_handler;
	sighandler.sa_flags = SA_RESETHAND | SA_RESTART;
	sigemptyset(&bl);
	sigaddset(&bl, SIGQUIT);
	sighandler.sa_mask = bl;
	if (sigaction(SIGINT, &sighandler, NULL) == -1)
	{
		printf(" sigaction initialization error\n");
		return EXIT_FAILURE;
	}

	strcpy(url, GETMARKETHISTORY);

	vol = init_vol();
	if (vol == NULL)
	{
		printf(" init_vol error \n");
		return EXIT_FAILURE;
	}
	wait_until(STARTING_TS);	

	idx = 0;
	while (h)
	{
		handle = curl_easy_init();
		if (handle)
		{	
			idx = 0;
			while (idx < MAX_TICKERS && h)
			{
				strcpy(url + MARKETH_LEN, tickers[idx]);
				get_history(url, handle, vol + idx, &td);
				idx++;
			}
			curl_easy_cleanup(handle);
		}
	}

	idx = 0;
	while (idx < MAX_TICKERS)
	{
		fclose((vol + idx)->five.fd);
		fclose((vol + idx)->fifteen.fd);
		idx++;
	}
	free(vol);
	return EXIT_SUCCESS;
}

