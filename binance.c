#include <stdio.h>
#include <curl/curl.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#define t_market market
#define t_candle candle
#define s_market smarket
#define s_candle scandle
#include "btrx.h"
#undef t_candle
#undef s_candle
#undef t_market
#undef s_market
#include "binance.h"
#include <unistd.h>


unsigned char volatile h = 1;


void	sig_handler(int sig)
{
	h = 0;
	printf("[Signal handler]\n");
	
	(void)sig;
}

size_t	write_data(void *buffer, size_t size, size_t nmemb, void *userp)
{

	char	*t;
	input	*in = (input*)userp;

	if (in == NULL || buffer == NULL)
		return 0;
	if (in->size == 0)
	{
		in->size = nmemb;
		in->data = (char*)malloc(sizeof(char) * nmemb);
		memcpy((void*)in->data, buffer, nmemb);
	}
	else
	{
		t = (char*)malloc(sizeof(char) * (in->size + nmemb));
		memcpy(t, in->data, in->size);
		memcpy(t + in->size, buffer, nmemb);
		free(in->data);
		in->size += nmemb;
		in->data = t;
	}

	return nmemb;
	(void)size;
}

void	get_klines(input *buff, t_market *mk)
{
	size_t	idx = 1;
	size_t	n = 0;
	
	if (buff->size < 110)
	{
		printf("  Request error %s\n", symbols[mk->idx]);
		return;
	}

	while (idx < buff->size)
	{
		if (buff->data[idx] == '[')
		{
			idx += 16;
			mk->fifteen[n].open = atof(buff->data + idx);
			idx += 13;
			mk->fifteen[n].high = atof(buff->data + idx);
			idx += 13;
			mk->fifteen[n].low = atof(buff->data + idx);
			idx += 13;
			mk->fifteen[n].close = atof(buff->data + idx);
			idx += 13;
			mk->fifteen[n].vol = atoi(buff->data + idx);
			idx += 37;
			while (*(buff->data) != '"' && *(buff->data + idx + 1) != ',')
				idx++;
			idx += 2;
			mk->fifteen[n].trades = atoi(buff->data + idx);
			idx += 30;
			n++;
		}	
		idx++;
	}
}

int	update_market_status(CURL *handle, char *url, t_market *mk)
{
       	CURLcode	res;
	input		*buff;

	strcpy(url + KLINE_LEN, symbols[mk->idx]);
	strcat(url, "&interval=");
	strcat(url, INTERVAL);
	strcat(url, "&limit=");
	strcat(url, S_LIMIT);
	
	buff = (input*)malloc(sizeof(input));
	buff->size = 0;
	buff->data = NULL;
		
	curl_easy_setopt(handle, CURLOPT_URL, url);
	curl_easy_setopt(handle, CURLOPT_WRITEDATA, buff);
	
	res = curl_easy_perform(handle);
	if (res != CURLE_OK)
	{
		fprintf(stderr, "curl easy perform error %s[%s]\n", curl_easy_strerror(res), symbols[mk->idx]);
		if (buff->data)
			free(buff->data);
		free(buff);
		return 1;
	}
	//printf("%s[%ld]\n", url, mk->idx);
	get_klines(buff, mk);
	free(buff->data);
	free(buff);

	return 0;
}

void	check_market_status(t_market *mk)
{
	size_t 	n;
	double 	vma;
	double 	sma;
	size_t	idx = 0;
	double	temp;
	double	change;

	while (idx < SYMBOLS)
	{
		n = 0;
		vma = 0;
		sma = 0;
		while (n < I_LIMIT)
		{
			sma += (mk + idx)->fifteen[n].close;
			vma += (mk + idx)->fifteen[n].vol;
			n++;
		}
		vma = ((mk + idx)->fifteen[I_LIMIT - 1].vol * I_LIMIT) / vma;
		if (vma > VOL_LIMIT)
		{
			sma = sma / I_LIMIT;
			if ((mk + idx)->fifteen[I_LIMIT - 1].close > (mk + idx)->fifteen[I_LIMIT - 2].close)
			{
				temp = (mk + idx)->fifteen[I_LIMIT - 1].high - (mk + idx)->fifteen[I_LIMIT - 2].close;
				change = temp / ((mk + idx)->fifteen[I_LIMIT - 2].close * 0.01);
				if (change > 2.0)
					printf("\a\a\a");
				printf("\033[1;32m%s\t\t%.2lf\033[0m  [%.2lf%%]\n",symbols[idx], vma * 100, change);
			}
			else
			{
				temp = (mk + idx)->fifteen[I_LIMIT - 2].close - (mk + idx)->fifteen[I_LIMIT - 1].low;
				change = temp / ((mk + idx)->fifteen[I_LIMIT - 2].close * 0.01);
				printf("\a\033[1;31m%s\t\t%.2lf\033[0m  [%.2lf%%] \n",symbols[idx], vma * 100, change);
			}
		}
		idx++;
	}
	printf("\n\n");
}



int	fomo(void)
{	
	CURL		*handle;
	t_market	*mk;
	char		url[128];
	size_t		idx = 0;
	struct sigaction sighandler;
	sigset_t 	bl;

	// sigaction init
	sighandler.sa_handler = sig_handler;
	sighandler.sa_flags = SA_RESETHAND | SA_RESTART;

	sigemptyset(&bl);
	sigaddset(&bl, SIGQUIT);
	sighandler.sa_mask = bl;
	
	if (sigaction(SIGINT, &sighandler, NULL) == -1)
	{
		printf(" sigaction initialization error\n");
		return 1;
	}
	// init url & markets
	strcpy(url, EX_KLINES);
	mk = (t_market*)malloc(sizeof(t_market) * SYMBOLS);

	while (idx < SYMBOLS)
	{
		(mk + idx)->idx = idx;
		idx++;	
	}

	handle = curl_easy_init();
	if (handle)
	{
		curl_easy_setopt(handle, CURLOPT_WRITEFUNCTION, write_data);
		while (h)
		{
			idx = 0;
			while (idx < SYMBOLS && h)
			{
				if (update_market_status(handle, url, mk + idx))
				{
					curl_easy_cleanup(handle);
					return 1;
				}
				idx++;
			}
			check_market_status(mk);
		}
	}

	free(mk);	
	curl_easy_cleanup(handle);
	return 0;
}

int	main(int argc, char **argv)
{
	pid_t	pid = 0;

	if (argc > 1)
	{
		if (argv[1][0] == '-' && argv[1][1] == 'f')
		{
			pid = fork();
			if (pid > 0)
			{
				printf("\nChild process %d\n", pid);
				return 1;
			}
			else if (pid == 0)// child proc can be killed with "kill -n 2 <pid>"
			{
				printf("Child process running\n");
			}
			else
				printf("\n fork error\n");
		}
	}
	fomo();
	printf(" %s was stopped\n", argv[0]);
	
	return 0;
}
